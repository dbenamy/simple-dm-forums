# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field


class Forum(Item):
    link = Field()
    title = Field()


class Topic(Item):
    id = Field()
    url = Field()
    title = Field()


class Message(Item):
    id = Field()
    topic = Field()
    author = Field()
    datetime = Field()
    text = Field()
