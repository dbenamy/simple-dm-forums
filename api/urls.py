from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^update$', 'dragonforum.api.views.update'),
    url(r'^crawled_page$', 'dragonforum.api.views.crawled_page'),
    url(r'^is_page_crawled$', 'dragonforum.api.views.is_page_crawled'),
)
