from django.db import models


class PageCrawl(models.Model):
    url = models.URLField(primary_key=True)
    last_crawl = models.DateTimeField()
    
    def __unicode__(self):
        return u'%s, %s' % (self.last_crawl, self.url[:70])
